var Socket = require('./Engine/socket.js');
var Chat = require('./Engine/chat.js');
var Mongodb = require('./Engine/mongodb.js');
var Userserver = require('./Engine/userserver.js');
var Conf = require('./Engine/config.js');
var Log = require('./Engine/log.js');
var fs = require('fs');

var config = new Conf('config.json');

var date = new Date();
var log = new Log("\n-------------------\n  Starting server\n-------------------\n", 'log/' + date.getFullYear() + '.' + date.getMonth() + '.' + date.getDate() + '.txt');

var mongo = new Mongodb(config.mongodb);
var userv = new Userserver(mongo);

mongo.on('connect', function() {
    var sock = new Socket(config.socket);

    var chat = new Chat(sock, userv);
});

//require('node-monkey').start({host: "127.0.0.1", port:"1234"});
