var Log = require('./log.js');
var _scfg = require('./socket_cfg.js');
var fs = require('fs');

var config = function(filename) {
    if (filename[0] !== '/') {
        filename = '../' + filename;
    }

    var obj = require(filename);

    for (var i in this.__parseobj(obj)) {
        this[i] = obj[i];
    }

    return this;
}

config.prototype.__parseobj = function(obj) {
    for (var i in obj) {
        var otype = typeof obj[i];

        if (otype === "object") {
            obj[i] = this.__parseobj(obj[i]);
        } else if (otype === "string" && obj[i].length > 0) {
            if (!isNaN(obj[i])) {
                if (obj[i] % 1 === 0)
                    obj[i] = parseInt(obj[i]);
                else
                    obj[i] = parseFloat(obj[i]);
            } else {
                var match = obj[i].match(/([a-zA-z]+):(.*)?/);
                if (match !== null) {
                    switch (match[1]) {
                        case 'f':
                            obj[i] = fs.readFileSync(match[2]);
                            break;
                        default:
                            obj[i] = match[2];
                            if (_scfg.debug) {
                                Log("unknown config option: " + match[i] + ":");
                                throw "unknown config option";
                            }
                            break;
                    }
                }
            }
        }
    }

    return obj;
}

module.exports = config;
