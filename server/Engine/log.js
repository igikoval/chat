var fs = require('fs');

var instance = null;

function log(text, filename, callback, hidden) {
    if (filename === true || callback === true)
        hidden = true;

    if (typeof callback === "string")
        filename = callback;

    if (typeof filename === "function")
        callback = filename;

    if (instance === null) {
        instance = this;

        if (typeof filename !== "string")
            filename = 'log.txt';

        this.filename = filename;
        if (typeof text !== "undefined")
            this.write(text, callback, hidden);

        return this;

    } else if (typeof text !== "undefined") {
        if (typeof filename === "string")
            instance.filename = filename;

        return instance.write(text, callback, hidden);
    }
}

log.prototype.write = function(text, callback, hidden) {
    if (!hidden)
        console.log(text);

    var date = new Date();
    var date_text = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

    fs.appendFile(this.filename, '[' + date_text + '] ' + text + "\n", function(err) {
        if(err) {
            console.log(err);
            throw "log save error";
        }

        if (typeof callback === "function")
            calback();
    });
}

module.exports = log;
