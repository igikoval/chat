var Log = require('./log.js');
var _scfg = require('./socket_cfg.js');

var message = function(msg) {
    this.types = {
        'outgoing'  : 0,
        'private'   : 1,
        'passing'   : 2,
        'broadcast' : 3,
        'friendlist': 4
    }

    if (typeof msg === "undefined" || typeof this.parse(msg).text === "undefined") {
        this.code   = 0;
        this.text   = "";
        this.parsed = false;
        this.type   = this.types.outgoing;
    }

    return this;
}

message.prototype.parse = function(msg) {
    if (typeof msg === "string" && msg.length >= _scfg.bits) {
        this.code = _scfg.parse_num(msg.substr(0, _scfg.bits));
        this.text = (msg.length > _scfg.bits) ? msg.substr(_scfg.bits) : '';

        this.type   = this.types.private;

        if (this.code < 5) {
            this.type = this.types.broadcast;

        } else if (this.code >= 9 && this.code < 20) {

            this.type     = this.types.passing;
            var reclen = _scfg.parse_num(this.text.substr(0, _scfg.id_bits));
            var pos = _scfg.id_bits;
            this.receiver = this.text.substr(pos, reclen);
            this.text     = this.text.substr(pos + reclen);
            if (this.receiver == -1)
                if (this.code < 14)
                    this.type = this.types.broadcast;
                else {
                    if (_scfg.debug) {
                        Log("msg receiver required");
                        Log(this, true);
                        throw "msg receiver required";
                    }
                }

        } else if (this.code >= 20) {
            this.type = this.types.friendlist;
        }

        this.parsed = true;
    }

    return this;
}


message.prototype.push = function(text, bits) {
    if (typeof text !== "string")
        if (typeof text.toString === "function")
            text = text.toString();
        else
            throw "unpushable text";

    if (typeof bits !== "undefined") {
        var nan = isNaN(text);
        while (text.length < bits) {
            if (nan) {
                text = ' ' + text;
            } else {
                text = '0' + text;
            }
        }
    }

    this.text += text;

    return this;
}


message.prototype.toString = function() {
    return _scfg.fill_bits(this.code,_scfg.bits) + this.text
}


module.exports = message;
