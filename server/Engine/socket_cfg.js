var scfg = {
    debug    : true,
    codes    : {
        0 : 'connect',
        1 : 'close',

        5 : 'login',
        6 : 'register',
        7 : 'status',

        9 : 'msg',


        14 : 'startbuff',
        15 : 'delbuff',
        16 : 'addletter',
        17 : 'delletter',
        18 : 'setbuff',
        19 : 'sendbuff',

        20 : 'friendlist',
        21 : 'addfriend',
        22 : 'delfriend'
    },

    /** kody nie wymagające logowania */
    nl_codes : [5, 6],

    bits     : 4,
    id_bits  : 4,

    salt     : 'asdfg',
    s_salt   : 'ddghk',

    fill_bits      : function (text, bits) {
        if (typeof text === "number")
            text = text.toString();

        while (text.length < bits)
            text = '0' + text;

        return text;
    },
    parse_num      : function (text) {
        return parseInt(text.replace(/.*\-/, '-'));
    }
}

scfg.codenames = {};
for (var i in scfg.codes) {
    scfg.codenames[scfg.codes[i]] = parseInt(i);
}

module.exports = scfg;
