var util = require('util');
var EventEmitter = require('events').EventEmitter;
var MongoClient = require('mongodb').MongoClient;

var mongodb = function(config) {
    var self = this;
    this.url = 'mongodb://' + config.address + ':' + config.port + '/' + config.dbname;
    this.prefix = config.prefix;
    this.db = null;

    MongoClient.connect(this.url, function(err, db) {
        if (err !== null)
            throw err;

        self.db = db;
        self.emit('connect');
    });

    return this;
};

util.inherits(mongodb, EventEmitter);

mongodb.prototype.close = function() {
    if (this.db !== null) {
        this.db.close();
        this.db = null;
    }

    return this;
}

mongodb.prototype.collection = function(name) {
    return this.db.collection(this.prefix + name);
}

module.exports = mongodb;
