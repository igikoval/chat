var Log = require('./log.js');
var util = require('util');
var EventEmitter = require('events').EventEmitter;
var _scfg = require('./socket_cfg.js');

var userserver = function(mongodb) {
    this.mongo = mongodb;

    return this;
}

util.inherits(userserver, EventEmitter);

userserver.prototype.checklogin = function (user, hash) {
    var self = this;

    return new Promise(function (resolve, reject) {
        if (user.length === 0 || hash.length === 0)
            return reject();

        self.mongo.collection('user').findOne({"name" : user}).then(function(doc){
            if (doc === null)
                return reject();
            else if (doc.hash === hash)
                return resolve();
            else
                return reject();
        }, function(){
            reject();
        });
    });
};

userserver.prototype.register = function(user, hash) {
    var self = this;

    return new Promise(function(resolve, reject) {
        if (user.length === 0 || hash.length === 0)
            return reject();

        self.mongo.collection('user').findOne({"name" : user}).then(function(doc){
            if (doc !== null)
                return reject();

            self.mongo.collection('user').insertOne({"name" : user, "hash" : hash}).then(function(){
                resolve();
            }, function() {
                reject();
            });
        }, function(){
            reject();
        });
    });
}

userserver.prototype.fl_friendlist = function (msg, conn) {
    var self = this;

    return new Promise(function (resolve, reject) {
        self.mongo.collection('friendlist')
            .findOne( { "owner" : conn.user } )
            .then(
            function(doc) {
                var friendlist = [];

                for (var i in doc) {
                    if (i !== "_id" && i !== "owner")
                        friendlist.push(doc[i]);
                }

                resolve(friendlist);
            },
            function() {

                reject();
            });
    });
}

module.exports = userserver;
