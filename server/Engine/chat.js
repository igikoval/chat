var md5 = require('md5');
var util = require('util');
var EventEmitter = require('events').EventEmitter;
var Message = require('./message.js');
var Log = require('./log.js');
var _scfg = require('./socket_cfg.js');

var chat = function(socket, userv) {
    var self = this;
    this.sock = socket;
    this.userv = userv;

    this._onsmessage = function(msg, conn) {
        if (typeof msg !== "object" || typeof msg.code === "undefined" || typeof _scfg.codes[msg.code] === "undefined") {
            if (_scfg.debug) {
                Log('Unsupported message code or malformed message');
                Log(msg);
                throw "Unsupported message";
            }
        }

        if (_scfg.nl_codes.indexOf(msg.code) < 0 && !conn.logged) {

            if (_scfg.debug) {
                Log('Unpermitted message: ' + _scfg.codes[msg.code] + ' [code ' + msg.code + ']');
                throw "unpermitted message";
            }
            return false;

        }

        if (msg.type === msg.types.passing) {
            self.emit('msg', msg, conn);

        } else if (msg.type === msg.types.friendlist) {

            var fname = 'fl_' + _scfg.codes[msg.code];

            if (typeof self.userv[fname] === "function") {
                self.userv[fname](msg, conn).then(function(res){
                    if (typeof res !== "undefined") {

                        var sendmsg = new Message();
                        sendmsg.code = msg.code;
                        sendmsg.push(JSON.stringify(res));

                        conn.send(sendmsg);
                    }
                });
            }
        } else {
            var codename = _scfg.codes[msg.code];
            self.emit(codename, msg, conn);
        }
    }

    socket.on('text', this._onsmessage);
    for (var i in _scfg.codes) {
        var codename = _scfg.codes[i];
        var funcname = '_on' + codename;

        if (typeof this[funcname] === "function") {
            this.on(codename, this[funcname]);
        }
    }
}

util.inherits(chat, EventEmitter);

chat.prototype._onmsg = function(msg, conn) {
    var sendmsg = new Message();
    sendmsg.code = msg.code;
    sendmsg.push(conn.user.length, _scfg.id_bits)
            .push(conn.user)
            .push(msg.text);

    this.sock.sendto(sendmsg, msg.receiver);
}

chat.prototype._onlogin = function(msg, conn) {
    var scmd = msg.text.substr(0, 4);
    var sparam = msg.text.substr(4);

    if (typeof conn.loginparts === "undefined")
        conn.loginparts = 0;


    switch (scmd) {
        case 'user':
            conn.logged = false;
            conn.user = sparam;
            conn.loginparts++;
            break;
        case 'hash':
            conn.logged = false;
            conn.hash = md5(sparam + _scfg.s_salt);
            conn.loginparts++;
            break;
        default:
            if (_scfg.debug) {
                Log('Wrong login command: ' + scmd);
                Log(conn, true);
                throw "wrong command";
            }
            break;
    }

    if (conn.loginparts >= 2) {
        this.userv.checklogin(conn.user, conn.hash).then(function() {
            Log('login ' + conn.user + ' on id ' + conn.id, !_scfg.debug);
            var msg = new Message();
            msg.code = _scfg.codenames.login;
            conn.logged = true;
            conn.send(msg);

        }, function() {
            Log('login ' + conn.user + ' failed', !_scfg.debug);
            var msg = new Message();
            msg.code = _scfg.codenames.close;
            conn.send(msg);
            conn.close();
        });

        conn.loginparts = 0;
    }
}


chat.prototype._onregister = function(msg, conn) {
    var scmd = msg.text.substr(0, 4);
    var sparam = msg.text.substr(4);

    if (typeof conn.registerparts === "undefined")
        conn.registerparts = 0;


    switch (scmd) {
        case 'user':
            conn.logged = false;
            conn.user = sparam;
            conn.registerparts++;
            break;
        case 'hash':
            conn.logged = false;
            conn.hash = md5(sparam + _scfg.s_salt);
            conn.registerparts++;
            break;
        default:
            if (_scfg.debug) {
                Log('Wrong login command: ' + scmd);
                Log(conn, true);
                throw "wrong command";
            }
            break;
    }

    if (conn.registerparts >= 2) {
        this.userv.register(conn.user, conn.hash).then(function() {
            Log('register ' + conn.user + ' on id ' + conn.id, !_scfg.debug);
            var msg = new Message();
            msg.code = _scfg.codenames.register;
            conn.send(msg);

        }, function() {
            Log('register ' + conn.user + ' failed', !_scfg.debug);
            var msg = new Message();
            msg.code = _scfg.codenames.close;
            conn.send(msg);
            conn.close();
        });

        conn.registerparts = 0;
    }
}


chat.prototype._onstatus = function(msg, conn) {
    var target = false;
    for (var i in this.sock.clients) {
        if (this.sock.clients[i].user === msg.text) {
            target = this.sock.clients[i];
            break;
        }
    }

    var sendmsg = new Message();
    sendmsg.code = msg.code
    sendmsg.push(msg.text.length, _scfg.id_bits);
    sendmsg.text += msg.text;
    sendmsg.text += (target === false || !target.logged) ? "false" : "true";


    conn.send(sendmsg);
}


module.exports = chat;
