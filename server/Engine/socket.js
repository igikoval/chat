var fs = require('fs');
var ws = require("nodejs-websocket");
var util = require('util');
var _scfg = require('./socket_cfg.js');
var Client = require('./client.js');
var Message = require('./message.js');
var Log = require('./log.js');
var EventEmitter = require('events').EventEmitter;


/**
 * Kontroler socketa
 * @param  {Object} config Server's configuration
 * @constructor
 */
var socket = function(config) {
    var self     = this;
    this.port    = (typeof config.port === "string" || typeof config.port === "number") ? config.port : defaults.port;
    this.address = (typeof config.address === "string") ? config.address : defaults.address;
    this.config  = config;
    this.clients = [];
    this.names   = [];

    this.server = ws.createServer(config,function(conn){
        var free_id = self.free_id();
        if (free_id !== false) {
            conn.id = free_id;
            self.clients[free_id] = conn;
        } else
            conn.id = self.clients.push(conn) - 1;

        conn._send = conn.send;
        conn.send = function(msg) {
            return this._send(msg.toString());
        }

        conn.send(new Message(_scfg.fill_bits(0, _scfg.bits) + _scfg.salt));


        conn.on('text', function(text){
            var msg = new Message(text);
            self.emit('text', msg, conn);
        });

        conn.on('close', function(){
            delete self.names[conn.user];
            delete self.clients[conn.id];

            self.emit('disconnect', conn);
        });

        Log('New connection id: ' + conn.id + ' from ' + conn.socket.remoteAddress, !_scfg.debug);


        self.emit('connect', conn);
    }).listen(
        this.port,
        this.address
    );
    Log('Listening on: ' + this.address + ' port: ' + this.port, !_scfg.debug);
    Log('Secure socket: ' + ((config.secure) ? 'enabled' : 'disabled'), !_scfg.debug);

    this.sendto = this.server.sentdo = function(msg, receiver) {
        for (var i in self.clients) {
            if (typeof self.clients[i] === "undefined")
                continue;

            var sock = self.clients[i];

            if (sock.user === receiver) {
                sock.send(msg);
            }
        }
        return self;
    }

    this.broadcast = this.server.broadcast = function(msg, sender) {
        for (var i in self.clients) {
            if (typeof self.clients[i] === "undefined")
                continue;

            var sock = self.clients[i];

            if (i !== sender && sock !== null && sock.readyState === sock.OPEN && sock.logged)

                try {
                    sock.send(msg);
                } catch (e) {
                    if (_scfg.debug) {
                        Log('Broadcast exception');
                        Log(e, true);
                        throw e;
                    }
                }
        }
    }
}

util.inherits(socket, EventEmitter);

/**
 * Szuka najniższego pustego id
 * @return {Number|Boolean} Numer najniższego wolnego id lub false, jeśli trzeba dodać na końcu
 * @memberof socket
 */
socket.prototype.free_id = function() {
    for (var i = 0; i < this.clients.length; i++) {
        if (typeof this.clients[i] === "undefined" || this.clients[i] === null)
            return i;
    }
    return false;
}

module.exports = socket;
