/**
 * mod/socket.js
 * @file Model obsługi socketów
 */

/**
 * Model obsługi socketów
 * @param  {String}        ip      Adres IP serwera
 * @param  {String|Number} port    Port serwera
 * @memberof Chat.Mod
 * @constructor
 */
Chat.Mod.sock = function(port, ip) {
    Chat.Sys.class.call(this);

    if (typeof port === "undefined")
        port = Chat.Cfg.sock.port;

    if (typeof ip === "undefined")
        ip = Chat.Cfg.sock.ip;

    if (typeof WebSocket !== "function")
        throw "no websocket support";

    this.open = false;

    this.sock = new WebSocket('ws' + ((Chat.Cfg.sock.secure) ? 's' : '') + '://' + ip + ':' + port);

    var self = this;
    this.sock.onopen = function() {

        this.onmessage = function(event) {
            self._msg(self, event);
        };

        this.onclose = function() {
            self.emit('close');
        };
    }

    return this;
}

/**
 * Zdarzenie połączenia z serwerem
 * @event Chat.Mod.sock#connect
 * @memberof Chat.Mod.sock
 */

 /**
  * Zdarzenie rozłączenia z serwerem
  * @event Chat.Mod.sock#close
  * @memberof Chat.Mod.sock
  */

/**
 * Zdarzenie logowania
 * @event Chat.Mod.sock#login
 * @memberof Chat.Mod.sock
 */

 /**
  * Zdarzenie rejestracji
  * @event Chat.Mod.sock#register
  * @memberof Chat.Mod.sock
  */

  /**
   * Zdarzenie wiadomości
   * @event Chat.Mod.sock#msg
   * @memberof Chat.Mod.sock
   */

/**
 * Event wiadomości od socketa
 * @param {Chat.Mod.sock} self
 * @param MessageEvent      event
 * @listens WebSocket#onmessage
 */
Chat.Mod.sock.prototype._msg = function(self, event) {

    var msg  = new Chat.Mod.msg(event.data);
    var code = msg.code;

    if (!self.open) {
        if (code === parseInt(Chat.Cfg.sock.codes['connect'])) {
            self.salt = msg.body;
            self.open = true;
        }
    }


    if (self.open) {
        self.emit(Chat.Cfg.sock.codenames[code], [msg, event]);
    }
}


/**
 * Wysłanie wiadomości do serwera
 * @param  {Chat.Mod.msg} msg Wiadomość
 * @memberof Chat.Mod.sock
 */
Chat.Mod.sock.prototype.send = function (msg) {

    this.sock.send(msg.fill_bits(msg.code, Chat.Cfg.sock.bits)
            + msg.body);

    return this;
}

/**
 * Zamknięcie połączenia
 * @memberof Chat.Mod.sock
 */
Chat.Mod.sock.prototype.close = function() {
    this.sock.close();
}
