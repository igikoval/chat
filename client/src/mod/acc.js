/**
 * Obsługa konta
 * @param  {Chat.Mod.sock} sock Socket
 * @param  {string}        name Nazwa użytkownika
 * @param  {string}        pass Hasło
 * @constructor
 * @memberof Chat.Mod
 */
Chat.Mod.acc = function(sock, name, pass) {
    Chat.Sys.class.call(this);
    var self = this;
    this.sock = sock;
    this.logged = false;


    if (typeof name === "string" && typeof pass === "string") {
        if (!sock.open) {
            var onopen = function(){
                self._login(name, pass);
                sock.off('connect', onopen);
            };
            sock.on('connect', onopen);
        } else {
            this._login(name, pass);
        }

        /**
         * Zdarzenie logowania
         * @event Chat.Mod.acc#login
         * @memberof Chat.Mod.acc
         */
        this.addEvent('login');
    } else {
        this.register = function(login, password) {
            var self = this;

            return new Promise(function(resolve, reject) {
                var timeout = null;

                var onopen = function() {

                    sock.on('register', function() {
                        resolve();
                        clearTimeout(timeout);
                    });
                    sock.on('close', function() {
                        reject();
                        clearTimeout(timeout);
                    })
                    var hash = md5(password + sock.salt);
                    sock.send(new Chat.Mod.msg('user' + login, Chat.Cfg.sock.codes.register));
                    sock.send(new Chat.Mod.msg('hash' + hash,  Chat.Cfg.sock.codes.register));

                    sock.off('connect', onopen);
                };

                if (!sock.open) {
                    sock.on('connect', onopen);
                } else {
                    onopen();
                }

                timeout = setTimeout(function(){
                    reject();
                }, Chat.Cfg.sock.timeout);
            })
        }
    }

    return this;
}

/**
 * Loguje klienta
 * @param  {string} login Nazwa klienta
 * @param  {string} pass  Hasło
 * @memberof Chat.Mod.acc
 * @listens Chat.Mod.sock#login
 */
Chat.Mod.acc.prototype._login = function(login, pass) {
    var self = this;
    this.login = login;

    this.sock.on('login', function(msg, event){
        self._onlogin(msg, event);
    });

    var hash = md5(pass + this.sock.salt);
    this.sock.send(new Chat.Mod.msg('user' + login, Chat.Cfg.sock.codes.login));
    this.sock.send(new Chat.Mod.msg('hash' + hash,  Chat.Cfg.sock.codes.login));
}

/**
 * Czeka na informację o pomyślnym zalogowaniu
 * @fires Chat.Mod.acc#login
 */
Chat.Mod.acc.prototype._onlogin = function() {
    this.logged = true;
    this.emit('login');
    this.sock.off('login', this._onlogin);
}
