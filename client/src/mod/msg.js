/**
 * mod/msg.js
 * @file Klasa wiadomości
 */

/**
 * Wiadomość
 * @param  {String} txt Tekst do sparsowania
 * @memberof Chat.Mod
 * @constructor
 */
Chat.Mod.msg = function(txt, code) {

    this.code = -1;
    this.body = '';
    this.cursor = 0;
    var date = this.date = new Date();
    this.outgoing = true;

    this.dateStr = this.fill_bits(date.getHours(), 2) + ':' + this.fill_bits(date.getMinutes(), 2) + ':' + this.fill_bits(date.getSeconds(), 2);

    if (typeof code !== "undefined" && !isNaN(code)) {
        this.code = parseInt(code);
        this.body = txt;
    } else if (typeof txt !== "undefined")
        this.parse(txt);

    return this;
}


/**
 * Zbiera kilka bitów od pozycji kursora. Kursor przenosi się wraz ze ściąganiem bitów
 * @param  {Number} bits Ilość bitów do zdjęcia
 * @return {String}      Zdjęte bity
 */
Chat.Mod.msg.prototype.take = function(bits) {
    if (this.cursor >= this.body.length)
        return false;

    if (typeof bits === "undefined" || isNaN(bits))
        bits = this.body.length - this.cursor;

    var text = this.body.substr(this.cursor, bits);
    this.cursor += bits;
    return text;
}


/**
 * Wpycha kilka bitów na końcu wiadomości
 * @param  {String} txt  Treść
 * @param  {Number} bits Ilość bitów
 * @memberof Chat.Mod.msg
 */
Chat.Mod.msg.prototype.push = function(txt, bits) {

    if (typeof txt === "undefined")
        txt = "";

    if (typeof bits !== "undefined") {
        if (txt.length < bits)
            txt = '0' + txt;

        if (txt.length > bits)
            txt = txt.substr(0, bits);
    }

    this.body += txt;

    return this;
}

/**
 * Parsuje tekst wiadomości
 * @param  {String} txt Tekst
 * @memberof Chat.Mod.msg
 */
Chat.Mod.msg.prototype.parse = function(txt) {

    this.code = this.parseNum(txt.substr(0, Chat.Cfg.sock.bits));
    this.body = txt.substr(Chat.Cfg.sock.bits);
    this.outgogin = false;


    if (typeof Chat.Cfg.sock.codenames[this.code] === "undefined")
        throw "wrong msg";

    return this;
}


/**
 * Parsuje tekst na liczbę
 * @param  {String} txt
 * @return {Number}
 */
Chat.Mod.msg.prototype.parseNum = function(txt) {
    return parseInt(txt.replace(/.*\-/, '-'));
}


/**
 * Wypełnia kawałek wiadomości do odpowiedniej długości
 * @param  {String} text Tekst do wypełnienia
 * @param  {Number} bits Długość
 * @return {String}
 */
Chat.Mod.msg.prototype.fill_bits = function (text, bits) {
    if (typeof text === "number")
        text = text.toString();

    while (text.length < bits)
        text = '0' + text;

    return text;
}

/**
 * Tworzy właściwy ciąg znaków adresu odbiorcy
 * @param  {String} receiver Nazwa odbiorcy
 * @return {String}
 */
Chat.Mod.msg.prototype.receiver_str = function(receiver) {
    return (this.fill_bits(receiver.length, Chat.Cfg.sock.addr_bits) + receiver);
}
