// #define STATUSES __s
/**
 * Model obsługi statusów
 * @param  {Chat.Mod.sock} sock Socket do sprawdzania statusów
 * @constructor
 * @memberof Chat.Mod
 */
Chat.Mod.status = function(sock) {
    Chat.Sys.class.call(this);
    var self = this;
    this.sock = sock;
    this.STATUSES = {};
    this.ints = {};

    sock.on('status', function(msg, event){
        self._onstatus(msg, event);
    });

    /**
     * Zmiana statusu
     * @event Chat.Mod.status#change
     * @memberof Chat.Mod.status
     */
    this.addEvent('change');
    return this;
}

/**
 * Dodanie statusu do kolejki odświeżania
 * @param  {String} target Nazwa rozmówcy
 * @return {this}
 * @memberof Chat.Mod.status
 */
Chat.Mod.status.prototype.addStatus = function(target) {
    var self = this;
    this.STATUSES[target] = false;
    this.ints[target] = setInterval(function(){
        self.refreshStatus(target);
    }, Chat.Cfg.sock.s_timeout);
    this.refreshStatus(target);

    return this;
}

/**
 * Usuwa status z kolejki odświeżania
 * @param  {String} target Nazwa rozmówcy
 * @return {this}
 * @memberof Chat.Mod.status
 */
Chat.Mod.status.prototype.removeStatus = function(target) {
    if (typeof this.STATUSES[target] !== "undefined") {
        this.STATUSES[target] = false;
        clearInterval(this.ints[target]);
        delete this.ints[target];
        delete this.STATUSES[target];
    }

    return this;
}

/**
 * Odświeża status
 * @param  {String} target Nazwa rozmówcy
 * @return {this}
 * @memberof Chat.Mod.status
 */
Chat.Mod.status.prototype.refreshStatus = function(target) {
    var msg = new Chat.Mod.msg(target, Chat.Cfg.sock.codes.status);
    this.sock.send(msg);

    return this;
}

/**
 * Zmiana statusu
 * @param  {Chat.Mod.msg} msg   Wiadomość
 * @param  {MessageEvent} event Cały event wiasomości
 * @return {this}
 * @memberof Chat.Mod.status
 * @listens Chat.Mod.sock#status
 * @fires Chat.Mod.Status#change
 */
Chat.Mod.status.prototype._onstatus = function(msg, event) {
    var abits = msg.parseNum(msg.take(Chat.Cfg.sock.addr_bits));
    var target = msg.take(abits);
    var status = JSON.parse(msg.take());

    if (status !== this.STATUSES[target])
        this.emit('change', [target, status])

    this.STATUSES[target] = status;
}
