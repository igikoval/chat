// #if _CONTROLLER = angular
/**
 * Angularowy kontroler obsługi chata
 * @param  {ChildScope} $scope  Angularowy $scope
 * @constructor
 * @memberof Chat.Ctrl
 */
Chat.Ctrl.ngapp = function($scope, wrap) {
    Chat.Sys.class.call(this);
    var self = this;

    this.scope = $scope;
    this.wrap = wrap;
    this.chat = null;

    $scope._ctrl = this;

    /**
     * Czy użytkownik jest zalogowany
     * @type {Boolean}
     */
    $scope.loggedIn         = false;

    /**
     * Otwarte rozmowy
     * @type {Array}
     */
    $scope.chats            = [];

    /**
     * Aktywna rozmowa
     * @type {Object|null}
     */
    $scope.currentChat      = null;

    /**
     * Czy pokazać okno rozmowy
     * @type {Boolean}
     */
    $scope.showMsgBox       = false;

    /**
     * Złe dane do logowania
     * @type {Boolean}
     */
    $scope.wrongLogin       = false;

    /**
     * Połączenie zostało zamknięte
     * @type {Boolean}
     */
    $scope.closedConnection = false;


    /**
     * Włączenie logowania
     */
    $scope.submit_login = function() {
        self.chat = new Chat($scope.username, $scope.password);
        $scope.password         = "";
        $scope.wrongLogin       = false;
        $scope.closedConnection = false

        var onlogin = function(){
            $scope.$apply(function(){
                $scope.wrongLogin = false;
            });

            self.chat.off('login', onlogin);
            self.chat.SOCKET.off('close', onclose);
            self.chat.on('msg', function(msg){
                self._msg(msg);
            });

            self.emit('login');
            self.chat.SOCKET.on('close', function() {
                delete self.chat;

                $scope.$apply(function(){
                    $scope.chats            = [];
                    $scope.loggedIn         = false;
                    $scope.closedConnection = true;
                });
            });
        }

        var onclose = function(){
            $scope.$apply(function(){
                $scope.wrongLogin = true;
            });

            self.chat.off('login', onlogin);
            self.chat.SOCKET.off('close', onclose);
        }

        self.chat.on('login', onlogin);
        self.chat.SOCKET.on('close', onclose);

        self.chat.status.on('change', function(target, status){
            self._status(target, status);
        });
    }

    $scope.start_register = function() {
        self.emit('register');
    }

    $scope.logout = function() {
        self.chat.close();
        $scope.isMenuCollapsed = true;
    }

    /**
     * Rozpoczęcie nowej rozmowy
     * @param  {String} target Adresat
     * @return {Object} Rozmowa, która została otwarta
     */
    $scope.new_chat = function(target) {
        if (typeof target === "undefined") {
            target = $scope.newMsgUsername;
            $scope.newMsgUsername = "";
        }

        if (target.length === 0)
            return;

        for (var i in $scope.chats) {
            var chat = $scope.chats[i];

            if (chat.target === target) {
                $scope.currentChat = chat;
                chat.unread        = 0;
                $scope.showMsgBox  = true;
                return;
            }
        }

        var chat = {
            target: target,
            _ctrl : self,
            _log  : [],
            status: false,
            unread: 0
        };
        self.chat.status.addStatus(target);

        $scope.chats.push(chat);

        $scope.currentChat = chat;
        $scope.showMsgBox  = true;

        self.emit('new_msg', chat);

        return chat;
    }


    /**
     * Wysłanie wiadomości
     *   Wysyła wiadomość z modelu msgText
     */
    $scope.send_msg = function() {
        if ($scope.currentChat === null)
            return;

        var msg   = self.chat.msg ( $scope.currentChat.target, $scope.msgText );
        var login = self.chat.acc.login;

        msg.text = $scope.msgText;
        msg.sender = login;
        $scope.currentChat._log.push(msg);

        $scope.msgText = "";
    }


    /**
     * Obsługa puszczenia przycisku w oknie wiadomości
     *    Głównie dla obsługi entera
     * @param  {KeyEvent} event
     */
    $scope.msgTextKeyup = function(event) {
        if (event.keyCode === 13 && event.ctrlKey === false)
            $scope.send_msg();
    }

    /**
     * Zamknięcie rozmowy
     * @param  {Object} chat Rozmowa do zakmnięcia
     */
    $scope.chat_close = function(chat) {
        for (var i in $scope.chats) {
            if ($scope.chats[i].target === chat.target) {
                self.chat.status.removeStatus(chat.target);
                if (chat.target === $scope.currentChat.target) {
                    $scope.currentChat = null;
                    $scope.showMsgBox  = false;
                }
                $scope.chats.splice(i, 1);
                break;
            }
        }
    }


    this.addEvent('new_msg');
    this.addEvent('login');
    this.addEvent('register');
    this.on('login', function(){
        self.login();
    });
    return this;
}

/**
 * Przełącza status zalogowania
 * @memberof Chat.Ctrl.ngapp
 */
Chat.Ctrl.ngapp.prototype.login = function() {
    var self = this;
    this.scope.$apply(function() {
        self.scope.loggedIn = true;
    });
}

/**
 * Odbiera wiadomości
 * @param  {Chat.Mod.msg} msg Wiadomość
 * @memberof Chat.Ctrl.ngapp
 * @listens Chat.Mod.sock#msg
 */
Chat.Ctrl.ngapp.prototype._msg = function(msg) {
    var self = this;
    var chat = false;
    for (var i in this.scope.chats) {
        if (msg.sender === this.scope.chats[i].target) {
            chat = self.scope.chats[i];
            break;
        }
    }

    if (chat === false) {
        chat = self.scope.new_chat(msg.sender);
    }

    this.scope.$apply(function(){
        if (self.scope.currentChat.target !== msg.sender)
            chat.unread++;
        chat._log.push(msg);
    });


    this.emit('msg', [ msg ] );
}

/**
 * Zmienia status użytkownika
 * @param  {String}  target Nazwa użytkownika
 * @param  {Boolean} status Nowy status
 * @listens Chat.Mod.status#change
 */
Chat.Ctrl.ngapp.prototype._status = function(target, status) {
    for (var i in this.scope.chats) {
        var chat = this.scope.chats[i];
        if (target === chat.target) {
            this.scope.$apply(function(){
                chat.status = status;
            });
            break;
        }
    }
}

// #endif
