// #if _CONTROLLER = angular
/**
 * Angularowy kontroler obsługi rejestracji
 * @param  {ChildScope} $scope  Angularowy $scope
 * @constructor
 * @memberof Chat.Ctrl
 */
Chat.Ctrl.ngreg = function($scope, wrap) {
    Chat.Sys.class.call(this);
    var self = this;
    this.scope = $scope;
    this.wrap = wrap;

    $scope.registering = false;

    $scope.close = function() {
        $scope.registering = false;
    }

    $scope.submit = function() {
        var sock = new Chat.Mod.sock();
        var acc  = new Chat.Mod.acc(sock);

        acc.register($scope.username, $scope.password).then(function(){
            alert('Zarejestrowano poprawnie!');
            self.clear();
            sock.close();
        }, function() {
            alert('Nie udało się zarejestrować! Spróbuj innej nazwy użytkownika.');
            sock.close();
        });
    }
}

/**
 * Czyszczenie po rejestracji
 * @memberof Chat.Ctrl.ngreg
 */
Chat.Ctrl.ngreg.prototype.clear = function() {
    var ss = this.scope;
    this.scope.$apply(function(){
        ss.password  = '';
        ss.password2 = '';
        ss.registering = false;
    });
}
// #endif
