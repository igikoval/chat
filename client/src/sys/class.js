/**
 * sys/class.js
 * @file Plik standardowej klasy
 **/

/** Standardowa klasa, do lepszej obsługi klas :>
 * @memberof Chat.Sys
 * @constructor
 */
Chat.Sys.class = function() {
    for (var i in Chat.Sys.class.prototype) {
        if (typeof this[i] === "undefined")
            this[i] = Chat.Sys.class.prototype[i];
    }
    return this;
}

/**
 * Dodaje metodę pozwalającą pseudo-dziedziczyć z większej ilości klas
 * @param  {array|object} classes Tablica lub obiekt z klasami
 * @return {this}
 * @memberof Chat.Sys.class
 */
Chat.Sys.class.prototype.extends = function(classes) {
    for (var n in classes) {
        var obj = classes[n].obj;
        for (var i in obj.prototype) {
            if (typeof this[i] === "undefined")
                this[i] = obj.prototype[i];
        }
        obj.apply(this, classes[n].args);
    }

    return this;
}

/**
 * Dodaje zdarzenie do obiektu
 * @param  {String} name Nazwa eventu (np. move, click, etc...)
 * @return {this}
 * @memberof Chat.Sys.class
 */
Chat.Sys.class.prototype.addEvent = function(name) {
    this['on' + name] = new Chat.Sys.event(name);

    return this;
}

/**
 * Dodaję funkcję - słuchacza  do zdarzenia
 * @param  {String}   name Nazwa zdarzenia
 * @param  {Function} func Funkcja do wywołania
 * @return {this}
 * @memberof Chat.Sys.class
 */
Chat.Sys.class.prototype.on = function(name, func) {
    var propname = 'on' + name;

    if (typeof this[propname] === "undefined") {
        this.addEvent(name);
    }

    if (typeof this[propname] === "object") {
        this[propname].on(func);
    }

    return this;
}

/**
 * Wyzwala zdarzenie - uruchamia wszystkich słuchaczy
 * @param  {String} name Nazwa zdarzenia, które ma zostać wyzwolone
 * @param  {Object} args Argumenty wywoływanych funkcji
 * @return {this}
 * @memberof Chat.Sys.class
 */
Chat.Sys.class.prototype.emit = function(name, args) {
    if (typeof this['on' + name] === "object") {
        this['on' + name].emit(args);
    } else if (typeof this['on' + name] === "function") {
        this['on' + name].apply(this, args);
    }

    return this;
}

/**
 * Odpina funkcje-słuchaczy od zdarzenia
 * @param  {String} name Nazwa zdarzenia
 * @return {this}
 * @memberof Chat.Sys.class
 */
Chat.Sys.class.prototype.off = function(name, func) {
    if (typeof this['on' + name] === "object") {
        this['on' + name].off(func);
    }

    return this;
}
