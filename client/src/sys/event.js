/**
 * sys/event.js
 * @file Plik z klasą zdarzenia
 */

/**
 * Konstruktor standardowego zdarzenia
 * @memberof chat
 * @param  {String} name Nazwa eventu
 * @memberof Chat.Sys
 * @constructor
 */
Chat.Sys.event = function(name) {
    this.funcs = [];
    this.name = name;
}

/**
 * Przypina funkcję do zdarzenia
 * @param  {Function} funct Funkcja do podpięcia
 * @memberof Chat.Sys.event
 */
Chat.Sys.event.prototype.on = function(funct) {
    if (typeof funct === "function")
        this.funcs.push(funct);

    return this;
}

/**
 * Odpina funkcje od zdarzenia
 * @param {Function} funct Funkcja do odpięcia.
 *                         Jeśli nie podana, odpina wszystkie
 * @memberof Chat.Sys.event
 */
Chat.Sys.event.prototype.off = function(funct) {
    if (typeof funct === "undefined")
        this.funcs = [];
    else {
        var i = this.funcs.indexOf(funct);
        if (i > -1)
            this.funcs.splice(i, 1);
    }

    return this;
}

/**
 * Wyzwala zdarzenie
 * @param  {object} args Argumenty dla zdarzenia
 * @memberof Chat.Sys.event
 */
Chat.Sys.event.prototype.emit = function(args) {
    if (this.funcs.length > 0) {
        for (var i in this.funcs) {
            var func = this.funcs[i];
            func.apply(this, args);
        }
    }

    return this;
}
