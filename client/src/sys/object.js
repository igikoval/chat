/**
 * sys/object.js
 * @file Rozszerzenie standardowej klasy objektu
 */

/**
 * Dodaje obiekt do obiektu
 * @param  {Object}  obj
 * @memberof Object
 */
Object.prototype.push = function(obj) {
    if (typeof obj === "undefined")
        throw "function requires 1 parameter";

    for (var i in obj) {
        this[i] = obj[i];
    }

    return this;
}

/**
 * Obraca obiekt kluczami na wartości
 * @return {Object} obrócony klon obiektu
 */
Object.prototype.switch = function() {
    var obj = {};

    for (var i in this) {
        obj[this[i]] = i;
    }

    return obj;
}
