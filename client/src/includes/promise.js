// #if IE_SUPPORT = 1
if (typeof Promise === "undefined") {
    var Promise = window.Promise = function(cb) {
        return {
            then : function(resolve, reject) {
                if (typeof cb !== "function") {
                    if (typeof reject === "function")
                        return reject();
                    else
                        return;
                } else {
                    return cb(resolve, reject);
                }
            }
        }
    }
}
// #endif
