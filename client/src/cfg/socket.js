/**
 * cfg/socket.js
 * @file Konfiguracja socketa
 */

/**
 * Konfiguracja socketa
 * @memberof Chat.Cfg
 * @type {Object}
 */
// #define timeout _T
Chat.Cfg.sock = {
    /** domyślny adres ip serwera          */
    ip     : '192.168.1.100',
    /** domyślny port serwera              */
    port   : 1111,
    /** serwer zabezpieczony (wss)         */
    secure : true,

    /** timeout                            */
    timeout   : 30000,
    /** timeout statusu                    */
    s_timeout : 300000,

    /** maksymalna długość kodu wiadomości */
    bits      : 4,
    /** maksymalna ilość znaków długości
                nazwy użytkownika          */
    addr_bits : 4,

    /**
     * Nazwy kodów wiadomości
     * @type {Object}
     */
    codenames  : {
        0 : 'connect',
        1 : 'close',

        5 : 'login',
        6 : 'register',
        7 : 'status',

        9 : 'msg',

    // #if REALTIME = 1
        14 : 'startbuff',
        15 : 'delbuff',
        16 : 'addletter',
        17 : 'delletter',
        18 : 'setbuff',
        19 : 'sendbuff',
    // #endif

    // #if FRIENDLIST = 1
        20 : 'friendlist',
        21 : 'addfriend',
        22 : 'delfriend'
    // #endif
    }
};

Chat.Cfg.sock.codes = Chat.Cfg.sock.codenames.switch();
