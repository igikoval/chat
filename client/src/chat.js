"use strict;"

/**
 * Cały silnik chata
 * @namespace
 * @param {String}        username Nazwa użytkownika
 * @param {String}        password Hasło
 * @param {String|Number} port     Tylko jeśli DYNAMIC_IP = 1
 * @param {String}        ip       Tylko jeśli DYNAMIC_IP = 1
 * @constructor
 */
var Chat = function(username, password
    // #if DYNAMIC_IP = 1
        ,port, ip
    // #endif
    ) {
    var self = this;

    Chat.Sys.class.call(this);

    // #if DYNAMIC_IP = 1
    if (typeof port === "undefined")
        port = Chat.Cfg.sock.port;

    if (typeof ip === "undefined")
        ip = Chat.Cfg.sock.ip;

    this.SOCKET = new Chat.Mod.sock(port, ip);
    // #else
    this.SOCKET = new Chat.Mod.sock(Chat.Cfg.sock.port, Chat.Cfg.sock.ip);
    // #endif

    this.SOCKET.on('msg', function(msg){
        return self._onmsg(msg);
    });


    this.acc    = new Chat.Mod.acc(this.SOCKET, username, password);
    this.status = new Chat.Mod.status(this.SOCKET);

    this.acc.on('login', function() {
        self.emit('login');
    });




    /**
     * Zdarzenie wiadomości
     * @event Chat#msg
     * @memberof Chat
     */
    this.addEvent('msg');

    /**
     * Zdarzenie logowania
     * @event Chat#msg
     * @memberof Chat
     */
    this.addEvent('login');

    return this;
};

/**
 * Wysyła wiadomość
 * @param  {String} to   Nazwa odbiorcy
 * @param  {String} text Tekst wiadomośći
 * @memberof chat
 */
Chat.prototype.msg = function(to, text) {
    if (!this.acc.logged)
        throw "not logged";

    var self = this;

    var msg = new Chat.Mod.msg( '', Chat.Cfg.sock.codes.msg );

    msg.push(msg.receiver_str(to))
        .push(text);

    msg.target = to;


    this.SOCKET.send(msg);
    return msg;
}

/**
 * Listener odebrania wiadomości
 * @param  {Chat.Mod.msg} msg Odebrana wiadomość
 * @fires chat#msg
 * @memberof chat
 */
Chat.prototype._onmsg = function ( msg ) {

    var pos       = Chat.Cfg.sock.addr_bits;
    var addr_bits = parseInt(msg.body.substr( 0, pos ));
    msg.sender    = msg.body.substr( pos, addr_bits );

    pos += addr_bits
    msg.text      = msg.body.substr(pos);

    this.emit('msg', [ msg ] );
}

/**
 * Zamknięcie połączenia
 * @memberof Chat
 */
Chat.prototype.close = function() {
    this.SOCKET.close();
    delete this.acc;
    delete this.status;
    delete this.SOCKET;
}
