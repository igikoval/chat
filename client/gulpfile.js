var gulp = require('gulp');


var js_src = [
    'src/defines.jh',
    'src/includes/*.js',
    'src/chat.js',
    'src/sys.js',
    'src/sys/*.js',
    'src/cfg.js',
    'src/cfg/*.js',
    'src/mod.js',
    'src/mod/*.js',
    'src/ctrl.js',
    'src/ctrl/*.js'
];

/**
 * Kompilacja - łączenie plików w jeden i psucie
 */
gulp.task('compile', function(){

    var concat = require('gulp-concat'),
        rename = require('gulp-rename'),
	strip  = require('gulp-strip-comments'),
        uglify = require('gulp-uglify'),
        preprocess = require('gulp-preprocessor');

    return gulp.src(js_src)
        .pipe(concat('chat.js'))
        .pipe(preprocess())
        .pipe(strip())
        .pipe(gulp.dest('js'))
        .pipe(rename('chat.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

/**
 * Tworzenie dokumentacji     [description]
 */
gulp.task('docgen', function(){

    var gp_jsdoc = require('gulp-jsdoc3');

    return gulp.src(js_src)
        .pipe(gp_jsdoc())
})
