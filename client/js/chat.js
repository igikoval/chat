/**
 * defines.jh
 * @file Plik nagłówkowy, same definicje!
 */










    
    
    
    
    

function md5cycle(x, k) {
var a = x[0], b = x[1], c = x[2], d = x[3];

a = ff(a, b, c, d, k[0], 7, -680876936);
d = ff(d, a, b, c, k[1], 12, -389564586);
c = ff(c, d, a, b, k[2], 17,  606105819);
b = ff(b, c, d, a, k[3], 22, -1044525330);
a = ff(a, b, c, d, k[4], 7, -176418897);
d = ff(d, a, b, c, k[5], 12,  1200080426);
c = ff(c, d, a, b, k[6], 17, -1473231341);
b = ff(b, c, d, a, k[7], 22, -45705983);
a = ff(a, b, c, d, k[8], 7,  1770035416);
d = ff(d, a, b, c, k[9], 12, -1958414417);
c = ff(c, d, a, b, k[10], 17, -42063);
b = ff(b, c, d, a, k[11], 22, -1990404162);
a = ff(a, b, c, d, k[12], 7,  1804603682);
d = ff(d, a, b, c, k[13], 12, -40341101);
c = ff(c, d, a, b, k[14], 17, -1502002290);
b = ff(b, c, d, a, k[15], 22,  1236535329);

a = gg(a, b, c, d, k[1], 5, -165796510);
d = gg(d, a, b, c, k[6], 9, -1069501632);
c = gg(c, d, a, b, k[11], 14,  643717713);
b = gg(b, c, d, a, k[0], 20, -373897302);
a = gg(a, b, c, d, k[5], 5, -701558691);
d = gg(d, a, b, c, k[10], 9,  38016083);
c = gg(c, d, a, b, k[15], 14, -660478335);
b = gg(b, c, d, a, k[4], 20, -405537848);
a = gg(a, b, c, d, k[9], 5,  568446438);
d = gg(d, a, b, c, k[14], 9, -1019803690);
c = gg(c, d, a, b, k[3], 14, -187363961);
b = gg(b, c, d, a, k[8], 20,  1163531501);
a = gg(a, b, c, d, k[13], 5, -1444681467);
d = gg(d, a, b, c, k[2], 9, -51403784);
c = gg(c, d, a, b, k[7], 14,  1735328473);
b = gg(b, c, d, a, k[12], 20, -1926607734);

a = hh(a, b, c, d, k[5], 4, -378558);
d = hh(d, a, b, c, k[8], 11, -2022574463);
c = hh(c, d, a, b, k[11], 16,  1839030562);
b = hh(b, c, d, a, k[14], 23, -35309556);
a = hh(a, b, c, d, k[1], 4, -1530992060);
d = hh(d, a, b, c, k[4], 11,  1272893353);
c = hh(c, d, a, b, k[7], 16, -155497632);
b = hh(b, c, d, a, k[10], 23, -1094730640);
a = hh(a, b, c, d, k[13], 4,  681279174);
d = hh(d, a, b, c, k[0], 11, -358537222);
c = hh(c, d, a, b, k[3], 16, -722521979);
b = hh(b, c, d, a, k[6], 23,  76029189);
a = hh(a, b, c, d, k[9], 4, -640364487);
d = hh(d, a, b, c, k[12], 11, -421815835);
c = hh(c, d, a, b, k[15], 16,  530742520);
b = hh(b, c, d, a, k[2], 23, -995338651);

a = ii(a, b, c, d, k[0], 6, -198630844);
d = ii(d, a, b, c, k[7], 10,  1126891415);
c = ii(c, d, a, b, k[14], 15, -1416354905);
b = ii(b, c, d, a, k[5], 21, -57434055);
a = ii(a, b, c, d, k[12], 6,  1700485571);
d = ii(d, a, b, c, k[3], 10, -1894986606);
c = ii(c, d, a, b, k[10], 15, -1051523);
b = ii(b, c, d, a, k[1], 21, -2054922799);
a = ii(a, b, c, d, k[8], 6,  1873313359);
d = ii(d, a, b, c, k[15], 10, -30611744);
c = ii(c, d, a, b, k[6], 15, -1560198380);
b = ii(b, c, d, a, k[13], 21,  1309151649);
a = ii(a, b, c, d, k[4], 6, -145523070);
d = ii(d, a, b, c, k[11], 10, -1120210379);
c = ii(c, d, a, b, k[2], 15,  718787259);
b = ii(b, c, d, a, k[9], 21, -343485551);

x[0] = add32(a, x[0]);
x[1] = add32(b, x[1]);
x[2] = add32(c, x[2]);
x[3] = add32(d, x[3]);

}

function cmn(q, a, b, x, s, t) {
a = add32(add32(a, q), add32(x, t));
return add32((a << s) | (a >>> (32 - s)), b);
}

function ff(a, b, c, d, x, s, t) {
return cmn((b & c) | ((~b) & d), a, b, x, s, t);
}

function gg(a, b, c, d, x, s, t) {
return cmn((b & d) | (c & (~d)), a, b, x, s, t);
}

function hh(a, b, c, d, x, s, t) {
return cmn(b ^ c ^ d, a, b, x, s, t);
}

function ii(a, b, c, d, x, s, t) {
return cmn(c ^ (b | (~d)), a, b, x, s, t);
}

function md51(s) {
txt = '';
var n = s.length,
state = [1732584193, -271733879, -1732584194, 271733878], i;
for (i=64; i<=s.length; i+=64) {
md5cycle(state, md5blk(s.substring(i-64, i)));
}
s = s.substring(i-64);
var tail = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0];
for (i=0; i<s.length; i++)
tail[i>>2] |= s.charCodeAt(i) << ((i%4) << 3);
tail[i>>2] |= 0x80 << ((i%4) << 3);
if (i > 55) {
md5cycle(state, tail);
for (i=0; i<16; i++) tail[i] = 0;
}
tail[14] = n*8;
md5cycle(state, tail);
return state;
}

/* there needs to be support for Unicode here,
 * unless we pretend that we can redefine the MD-5
 * algorithm for multi-byte characters (perhaps
 * by adding every four 16-bit characters and
 * shortening the sum to 32 bits). Otherwise
 * I suggest performing MD-5 as if every character
 * was two bytes--e.g., 0040 0025 = @%--but then
 * how will an ordinary MD-5 sum be matched?
 * There is no way to standardize text to something
 * like UTF-8 before transformation; speed cost is
 * utterly prohibitive. The JavaScript standard
 * itself needs to look at this: it should start
 * providing access to strings as preformed UTF-8
 * 8-bit unsigned value arrays.
 */
function md5blk(s) { /* I figured global was faster.   */
var md5blks = [], i; /* Andy King said do it this way. */
for (i=0; i<64; i+=4) {
md5blks[i>>2] = s.charCodeAt(i)
+ (s.charCodeAt(i+1) << 8)
+ (s.charCodeAt(i+2) << 16)
+ (s.charCodeAt(i+3) << 24);
}
return md5blks;
}

var hex_chr = '0123456789abcdef'.split('');

function rhex(n)
{
var s='', j=0;
for(; j<4; j++)
s += hex_chr[(n >> (j * 8 + 4)) & 0x0F]
+ hex_chr[(n >> (j * 8)) & 0x0F];
return s;
}

function hex(x) {
for (var i=0; i<x.length; i++)
x[i] = rhex(x[i]);
return x.join('');
}

function md5(s) {
return hex(md51(s));
}

/* this function is much faster,
so if possible we use it. Some IEs
are the only ones I know of that
need the idiotic second function,
generated by an if clause.  */

function add32(a, b) {
return (a + b) & 0xFFFFFFFF;
}

if (md5('hello') != '5d41402abc4b2a76b9719d911017c592') {
function add32(x, y) {
var lsw = (x & 0xFFFF) + (y & 0xFFFF),
msw = (x >> 16) + (y >> 16) + (lsw >> 16);
return (msw << 16) | (lsw & 0xFFFF);
}
}


if (typeof Promise === "undefined") {
    var Promise = window.Promise = function(cb) {
        return {
            then : function(resolve, reject) {
                if (typeof cb !== "function") {
                    if (typeof reject === "function")
                        return reject();
                    else
                        return;
                } else {
                    return cb(resolve, reject);
                }
            }
        }
    }
}

"use strict;"

/**
 * Cały silnik chata
 * @namespace
 * @param {String}        username Nazwa użytkownika
 * @param {String}        password Hasło
 * @param {String|Number} port     Tylko jeśli DYNAMIC_IP = 1
 * @param {String}        ip       Tylko jeśli DYNAMIC_IP = 1
 * @constructor
 */
var Chat = function(username, password
    
    ) {
    var self = this;

    _csys.class.call(this);

    
    this.__s = new _cmod.sock(_scfg.port, _scfg.ip);
    
    this.__s.on('msg', function(msg){
        return self._onmsg(msg);
    });


    this.acc    = new _cmod.acc(this.__s, username, password);
    this.status = new _cmod.status(this.__s);

    this.acc.on('login', function() {
        self.emit('login');
    });




    /**
     * Zdarzenie wiadomości
     * @event Chat#msg
     * @memberof Chat
     */
    this.addEvent('msg');

    /**
     * Zdarzenie logowania
     * @event Chat#msg
     * @memberof Chat
     */
    this.addEvent('login');

    return this;
};

/**
 * Wysyła wiadomość
 * @param  {String} to   Nazwa odbiorcy
 * @param  {String} text Tekst wiadomośći
 * @memberof chat
 */
Chat.prototype.msg = function(to, text) {
    if (!this.acc.logged)
        throw "not logged";

    var self = this;

    var msg = new _cmod.msg( '', _scfg.codes.msg );

    msg.push(msg.receiver_str(to))
        .push(text);

    msg.target = to;


    this.__s.send(msg);
    return msg;
}

/**
 * Listener odebrania wiadomości
 * @param  {_cmod.msg} msg Odebrana wiadomość
 * @fires chat#msg
 * @memberof chat
 */
Chat.prototype._onmsg = function ( msg ) {

    var pos       = _scfg.addr_bits;
    var addr_bits = parseInt(msg.body.substr( 0, pos ));
    msg.sender    = msg.body.substr( pos, addr_bits );

    pos += addr_bits
    msg.text      = msg.body.substr(pos);

    this.emit('msg', [ msg ] );
}

/**
 * Zamknięcie połączenia
 * @memberof Chat
 */
Chat.prototype.close = function() {
    this.__s.close();
    delete this.acc;
    delete this.status;
    delete this.__s;
}

/**
 * Klasy systemowe
 * @memberof Chat
 * @namespace
 */
_csys = {};

/**
 * sys/class.js
 * @file Plik standardowej klasy
 **/

/** Standardowa klasa, do lepszej obsługi klas :>
 * @memberof _csys
 * @constructor
 */
_csys.class = function() {
    for (var i in _csys.class.prototype) {
        if (typeof this[i] === "undefined")
            this[i] = _csys.class.prototype[i];
    }
    return this;
}

/**
 * Dodaje metodę pozwalającą pseudo-dziedziczyć z większej ilości klas
 * @param  {array|object} classes Tablica lub obiekt z klasami
 * @return {this}
 * @memberof _csys.class
 */
_csys.class.prototype.extends = function(classes) {
    for (var n in classes) {
        var obj = classes[n].obj;
        for (var i in obj.prototype) {
            if (typeof this[i] === "undefined")
                this[i] = obj.prototype[i];
        }
        obj.apply(this, classes[n].args);
    }

    return this;
}

/**
 * Dodaje zdarzenie do obiektu
 * @param  {String} name Nazwa eventu (np. move, click, etc...)
 * @return {this}
 * @memberof _csys.class
 */
_csys.class.prototype.addEvent = function(name) {
    this['on' + name] = new _csys.event(name);

    return this;
}

/**
 * Dodaję funkcję - słuchacza  do zdarzenia
 * @param  {String}   name Nazwa zdarzenia
 * @param  {Function} func Funkcja do wywołania
 * @return {this}
 * @memberof _csys.class
 */
_csys.class.prototype.on = function(name, func) {
    var propname = 'on' + name;

    if (typeof this[propname] === "undefined") {
        this.addEvent(name);
    }

    if (typeof this[propname] === "object") {
        this[propname].on(func);
    }

    return this;
}

/**
 * Wyzwala zdarzenie - uruchamia wszystkich słuchaczy
 * @param  {String} name Nazwa zdarzenia, które ma zostać wyzwolone
 * @param  {Object} args Argumenty wywoływanych funkcji
 * @return {this}
 * @memberof _csys.class
 */
_csys.class.prototype.emit = function(name, args) {
    if (typeof this['on' + name] === "object") {
        this['on' + name].emit(args);
    } else if (typeof this['on' + name] === "function") {
        this['on' + name].apply(this, args);
    }

    return this;
}

/**
 * Odpina funkcje-słuchaczy od zdarzenia
 * @param  {String} name Nazwa zdarzenia
 * @return {this}
 * @memberof _csys.class
 */
_csys.class.prototype.off = function(name, func) {
    if (typeof this['on' + name] === "object") {
        this['on' + name].off(func);
    }

    return this;
}

/**
 * sys/event.js
 * @file Plik z klasą zdarzenia
 */

/**
 * Konstruktor standardowego zdarzenia
 * @memberof chat
 * @param  {String} name Nazwa eventu
 * @memberof _csys
 * @constructor
 */
_csys.event = function(name) {
    this.funcs = [];
    this.name = name;
}

/**
 * Przypina funkcję do zdarzenia
 * @param  {Function} funct Funkcja do podpięcia
 * @memberof _csys.event
 */
_csys.event.prototype.on = function(funct) {
    if (typeof funct === "function")
        this.funcs.push(funct);

    return this;
}

/**
 * Odpina funkcje od zdarzenia
 * @param {Function} funct Funkcja do odpięcia.
 *                         Jeśli nie podana, odpina wszystkie
 * @memberof _csys.event
 */
_csys.event.prototype.off = function(funct) {
    if (typeof funct === "undefined")
        this.funcs = [];
    else {
        var i = this.funcs.indexOf(funct);
        if (i > -1)
            this.funcs.splice(i, 1);
    }

    return this;
}

/**
 * Wyzwala zdarzenie
 * @param  {object} args Argumenty dla zdarzenia
 * @memberof _csys.event
 */
_csys.event.prototype.emit = function(args) {
    if (this.funcs.length > 0) {
        for (var i in this.funcs) {
            var func = this.funcs[i];
            func.apply(this, args);
        }
    }

    return this;
}

/**
 * sys/object.js
 * @file Rozszerzenie standardowej klasy objektu
 */

/**
 * Dodaje obiekt do obiektu
 * @param  {Object}  obj
 * @memberof Object
 */
Object.prototype.push = function(obj) {
    if (typeof obj === "undefined")
        throw "function requires 1 parameter";

    for (var i in obj) {
        this[i] = obj[i];
    }

    return this;
}

/**
 * Obraca obiekt kluczami na wartości
 * @return {Object} obrócony klon obiektu
 */
Object.prototype.switch = function() {
    var obj = {};

    for (var i in this) {
        obj[this[i]] = i;
    }

    return obj;
}

/**
 * Cała konfiguracja
 * @memberof Chat
 * @namespace
 */
_ccfg = {};

/**
 * cfg/_global.js
 * @file Globalna konfiguracja
 */

/**
 * Globalna konfiguracja
 * @memberof _ccfg
 * @type {Object}
 */

_ccfg._g = {

};

/**
 * cfg/socket.js
 * @file Konfiguracja socketa
 */

/**
 * Konfiguracja socketa
 * @memberof _ccfg
 * @type {Object}
 */

_scfg = {
    /** domyślny adres ip serwera          */
    ip     : '192.168.1.100',
    /** domyślny port serwera              */
    port   : 1111,
    /** serwer zabezpieczony (wss)         */
    secure : true,

    /** _T                            */
    _T   : 30000,
    /** _T statusu                    */
    s__T : 300000,

    /** maksymalna długość kodu wiadomości */
    bits      : 4,
    /** maksymalna ilość znaków długości
                nazwy użytkownika          */
    addr_bits : 4,

    /**
     * Nazwy kodów wiadomości
     * @type {Object}
     */
    codenames  : {
        0 : 'connect',
        1 : 'close',

        5 : 'login',
        6 : 'register',
        7 : 'status',

        9 : 'msg',

    
        14 : 'startbuff',
        15 : 'delbuff',
        16 : 'addletter',
        17 : 'delletter',
        18 : 'setbuff',
        19 : 'sendbuff',
    
    
        20 : 'friendlist',
        21 : 'addfriend',
        22 : 'delfriend'
        }
};

_scfg.codes = _scfg.codenames.switch();

/**
 * Modele
 * @memberof Chat
 * @namespace
 */
_cmod = {};

/**
 * Obsługa konta
 * @param  {_cmod.sock} sock Socket
 * @param  {string}        name Nazwa użytkownika
 * @param  {string}        pass Hasło
 * @constructor
 * @memberof _cmod
 */
_cmod.acc = function(sock, name, pass) {
    _csys.class.call(this);
    var self = this;
    this.sock = sock;
    this.logged = false;


    if (typeof name === "string" && typeof pass === "string") {
        if (!sock.open) {
            var onopen = function(){
                self._login(name, pass);
                sock.off('connect', onopen);
            };
            sock.on('connect', onopen);
        } else {
            this._login(name, pass);
        }

        /**
         * Zdarzenie logowania
         * @event _cmod.acc#login
         * @memberof _cmod.acc
         */
        this.addEvent('login');
    } else {
        this.register = function(login, password) {
            var self = this;

            return new Promise(function(resolve, reject) {
                var _T = null;

                var onopen = function() {

                    sock.on('register', function() {
                        resolve();
                        clearTimeout(_T);
                    });
                    sock.on('close', function() {
                        reject();
                        clearTimeout(_T);
                    })
                    var hash = md5(password + sock.salt);
                    sock.send(new _cmod.msg('user' + login, _scfg.codes.register));
                    sock.send(new _cmod.msg('hash' + hash,  _scfg.codes.register));

                    sock.off('connect', onopen);
                };

                if (!sock.open) {
                    sock.on('connect', onopen);
                } else {
                    onopen();
                }

                _T = setTimeout(function(){
                    reject();
                }, _scfg._T);
            })
        }
    }

    return this;
}

/**
 * Loguje klienta
 * @param  {string} login Nazwa klienta
 * @param  {string} pass  Hasło
 * @memberof _cmod.acc
 * @listens _cmod.sock#login
 */
_cmod.acc.prototype._login = function(login, pass) {
    var self = this;
    this.login = login;

    this.sock.on('login', function(msg, event){
        self._onlogin(msg, event);
    });

    var hash = md5(pass + this.sock.salt);
    this.sock.send(new _cmod.msg('user' + login, _scfg.codes.login));
    this.sock.send(new _cmod.msg('hash' + hash,  _scfg.codes.login));
}

/**
 * Czeka na informację o pomyślnym zalogowaniu
 * @fires _cmod.acc#login
 */
_cmod.acc.prototype._onlogin = function() {
    this.logged = true;
    this.emit('login');
    this.sock.off('login', this._onlogin);
}

/**
 * mod/msg.js
 * @file Klasa wiadomości
 */

/**
 * Wiadomość
 * @param  {String} txt Tekst do sparsowania
 * @memberof _cmod
 * @constructor
 */
_cmod.msg = function(txt, code) {

    this.code = -1;
    this.body = '';
    this.cursor = 0;
    var date = this.date = new Date();
    this.outgoing = true;

    this.dateStr = this.fill_bits(date.getHours(), 2) + ':' + this.fill_bits(date.getMinutes(), 2) + ':' + this.fill_bits(date.getSeconds(), 2);

    if (typeof code !== "undefined" && !isNaN(code)) {
        this.code = parseInt(code);
        this.body = txt;
    } else if (typeof txt !== "undefined")
        this.parse(txt);

    return this;
}


/**
 * Zbiera kilka bitów od pozycji kursora. Kursor przenosi się wraz ze ściąganiem bitów
 * @param  {Number} bits Ilość bitów do zdjęcia
 * @return {String}      Zdjęte bity
 */
_cmod.msg.prototype.take = function(bits) {
    if (this.cursor >= this.body.length)
        return false;

    if (typeof bits === "undefined" || isNaN(bits))
        bits = this.body.length - this.cursor;

    var text = this.body.substr(this.cursor, bits);
    this.cursor += bits;
    return text;
}


/**
 * Wpycha kilka bitów na końcu wiadomości
 * @param  {String} txt  Treść
 * @param  {Number} bits Ilość bitów
 * @memberof _cmod.msg
 */
_cmod.msg.prototype.push = function(txt, bits) {

    if (typeof txt === "undefined")
        txt = "";

    if (typeof bits !== "undefined") {
        if (txt.length < bits)
            txt = '0' + txt;

        if (txt.length > bits)
            txt = txt.substr(0, bits);
    }

    this.body += txt;

    return this;
}

/**
 * Parsuje tekst wiadomości
 * @param  {String} txt Tekst
 * @memberof _cmod.msg
 */
_cmod.msg.prototype.parse = function(txt) {

    this.code = this.parseNum(txt.substr(0, _scfg.bits));
    this.body = txt.substr(_scfg.bits);
    this.outgogin = false;


    if (typeof _scfg.codenames[this.code] === "undefined")
        throw "wrong msg";

    return this;
}


/**
 * Parsuje tekst na liczbę
 * @param  {String} txt
 * @return {Number}
 */
_cmod.msg.prototype.parseNum = function(txt) {
    return parseInt(txt.replace(/.*\-/, '-'));
}


/**
 * Wypełnia kawałek wiadomości do odpowiedniej długości
 * @param  {String} text Tekst do wypełnienia
 * @param  {Number} bits Długość
 * @return {String}
 */
_cmod.msg.prototype.fill_bits = function (text, bits) {
    if (typeof text === "number")
        text = text.toString();

    while (text.length < bits)
        text = '0' + text;

    return text;
}

/**
 * Tworzy właściwy ciąg znaków adresu odbiorcy
 * @param  {String} receiver Nazwa odbiorcy
 * @return {String}
 */
_cmod.msg.prototype.receiver_str = function(receiver) {
    return (this.fill_bits(receiver.length, _scfg.addr_bits) + receiver);
}

/**
 * mod/socket.js
 * @file Model obsługi socketów
 */

/**
 * Model obsługi socketów
 * @param  {String}        ip      Adres IP serwera
 * @param  {String|Number} port    Port serwera
 * @memberof _cmod
 * @constructor
 */
_cmod.sock = function(port, ip) {
    _csys.class.call(this);

    if (typeof port === "undefined")
        port = _scfg.port;

    if (typeof ip === "undefined")
        ip = _scfg.ip;

    if (typeof WebSocket !== "function")
        throw "no websocket support";

    this.open = false;

    this.sock = new WebSocket('ws' + ((_scfg.secure) ? 's' : '') + '://' + ip + ':' + port);

    var self = this;
    this.sock.onopen = function() {

        this.onmessage = function(event) {
            self._msg(self, event);
        };

        this.onclose = function() {
            self.emit('close');
        };
    }

    return this;
}

/**
 * Zdarzenie połączenia z serwerem
 * @event _cmod.sock#connect
 * @memberof _cmod.sock
 */

 /**
  * Zdarzenie rozłączenia z serwerem
  * @event _cmod.sock#close
  * @memberof _cmod.sock
  */

/**
 * Zdarzenie logowania
 * @event _cmod.sock#login
 * @memberof _cmod.sock
 */

 /**
  * Zdarzenie rejestracji
  * @event _cmod.sock#register
  * @memberof _cmod.sock
  */

  /**
   * Zdarzenie wiadomości
   * @event _cmod.sock#msg
   * @memberof _cmod.sock
   */

/**
 * Event wiadomości od socketa
 * @param {_cmod.sock} self
 * @param MessageEvent      event
 * @listens WebSocket#onmessage
 */
_cmod.sock.prototype._msg = function(self, event) {

    var msg  = new _cmod.msg(event.data);
    var code = msg.code;

    if (!self.open) {
        if (code === parseInt(_scfg.codes['connect'])) {
            self.salt = msg.body;
            self.open = true;
        }
    }


    if (self.open) {
        self.emit(_scfg.codenames[code], [msg, event]);
    }
}


/**
 * Wysłanie wiadomości do serwera
 * @param  {_cmod.msg} msg Wiadomość
 * @memberof _cmod.sock
 */
_cmod.sock.prototype.send = function (msg) {

    this.sock.send(msg.fill_bits(msg.code, _scfg.bits)
            + msg.body);

    return this;
}

/**
 * Zamknięcie połączenia
 * @memberof _cmod.sock
 */
_cmod.sock.prototype.close = function() {
    this.sock.close();
}


/**
 * Model obsługi statusów
 * @param  {_cmod.sock} sock Socket do sprawdzania statusów
 * @constructor
 * @memberof _cmod
 */
_cmod.status = function(sock) {
    _csys.class.call(this);
    var self = this;
    this.sock = sock;
    this.__s = {};
    this.ints = {};

    sock.on('status', function(msg, event){
        self._onstatus(msg, event);
    });

    /**
     * Zmiana statusu
     * @event _cmod.status#change
     * @memberof _cmod.status
     */
    this.addEvent('change');
    return this;
}

/**
 * Dodanie statusu do kolejki odświeżania
 * @param  {String} target Nazwa rozmówcy
 * @return {this}
 * @memberof _cmod.status
 */
_cmod.status.prototype.addStatus = function(target) {
    var self = this;
    this.__s[target] = false;
    this.ints[target] = setInterval(function(){
        self.refreshStatus(target);
    }, _scfg.s__T);
    this.refreshStatus(target);

    return this;
}

/**
 * Usuwa status z kolejki odświeżania
 * @param  {String} target Nazwa rozmówcy
 * @return {this}
 * @memberof _cmod.status
 */
_cmod.status.prototype.removeStatus = function(target) {
    if (typeof this.__s[target] !== "undefined") {
        this.__s[target] = false;
        clearInterval(this.ints[target]);
        delete this.ints[target];
        delete this.__s[target];
    }

    return this;
}

/**
 * Odświeża status
 * @param  {String} target Nazwa rozmówcy
 * @return {this}
 * @memberof _cmod.status
 */
_cmod.status.prototype.refreshStatus = function(target) {
    var msg = new _cmod.msg(target, _scfg.codes.status);
    this.sock.send(msg);

    return this;
}

/**
 * Zmiana statusu
 * @param  {_cmod.msg} msg   Wiadomość
 * @param  {MessageEvent} event Cały event wiasomości
 * @return {this}
 * @memberof _cmod.status
 * @listens _cmod.sock#status
 * @fires _cmod.Status#change
 */
_cmod.status.prototype._onstatus = function(msg, event) {
    var abits = msg.parseNum(msg.take(_scfg.addr_bits));
    var target = msg.take(abits);
    var status = JSON.parse(msg.take());

    if (status !== this.__s[target])
        this.emit('change', [target, status])

    this.__s[target] = status;
}

/**
 * Klasy kontrolerów
 * @memberof Chat
 * @namespace
 */
Chat.Ctrl = {};


/**
 * Angularowy kontroler obsługi chata
 * @param  {ChildScope} $scope  Angularowy $scope
 * @constructor
 * @memberof Chat.Ctrl
 */
Chat.Ctrl.ngapp = function($scope, wrap) {
    _csys.class.call(this);
    var self = this;

    this.scope = $scope;
    this.wrap = wrap;
    this.chat = null;

    $scope._ctrl = this;

    /**
     * Czy użytkownik jest zalogowany
     * @type {Boolean}
     */
    $scope.loggedIn         = false;

    /**
     * Otwarte rozmowy
     * @type {Array}
     */
    $scope.chats            = [];

    /**
     * Aktywna rozmowa
     * @type {Object|null}
     */
    $scope.currentChat      = null;

    /**
     * Czy pokazać okno rozmowy
     * @type {Boolean}
     */
    $scope.showMsgBox       = false;

    /**
     * Złe dane do logowania
     * @type {Boolean}
     */
    $scope.wrongLogin       = false;

    /**
     * Połączenie zostało zamknięte
     * @type {Boolean}
     */
    $scope.closedConnection = false;


    /**
     * Włączenie logowania
     */
    $scope.submit_login = function() {
        self.chat = new Chat($scope.username, $scope.password);
        $scope.password         = "";
        $scope.wrongLogin       = false;
        $scope.closedConnection = false

        var onlogin = function(){
            $scope.$apply(function(){
                $scope.wrongLogin = false;
            });

            self.chat.off('login', onlogin);
            self.chat.__s.off('close', onclose);
            self.chat.on('msg', function(msg){
                self._msg(msg);
            });

            self.emit('login');
            self.chat.__s.on('close', function() {
                delete self.chat;

                $scope.$apply(function(){
                    $scope.chats            = [];
                    $scope.loggedIn         = false;
                    $scope.closedConnection = true;
                });
            });
        }

        var onclose = function(){
            $scope.$apply(function(){
                $scope.wrongLogin = true;
            });

            self.chat.off('login', onlogin);
            self.chat.__s.off('close', onclose);
        }

        self.chat.on('login', onlogin);
        self.chat.__s.on('close', onclose);

        self.chat.status.on('change', function(target, status){
            self._status(target, status);
        });
    }

    $scope.start_register = function() {
        self.emit('register');
    }

    $scope.logout = function() {
        self.chat.close();
        $scope.isMenuCollapsed = true;
    }

    /**
     * Rozpoczęcie nowej rozmowy
     * @param  {String} target Adresat
     * @return {Object} Rozmowa, która została otwarta
     */
    $scope.new_chat = function(target) {
        if (typeof target === "undefined") {
            target = $scope.newMsgUsername;
            $scope.newMsgUsername = "";
        }

        if (target.length === 0)
            return;

        for (var i in $scope.chats) {
            var chat = $scope.chats[i];

            if (chat.target === target) {
                $scope.currentChat = chat;
                chat.unread        = 0;
                $scope.showMsgBox  = true;
                return;
            }
        }

        var chat = {
            target: target,
            _ctrl : self,
            _log  : [],
            status: false,
            unread: 0
        };
        self.chat.status.addStatus(target);

        $scope.chats.push(chat);

        $scope.currentChat = chat;
        $scope.showMsgBox  = true;

        self.emit('new_msg', chat);

        return chat;
    }


    /**
     * Wysłanie wiadomości
     *   Wysyła wiadomość z modelu msgText
     */
    $scope.send_msg = function() {
        if ($scope.currentChat === null)
            return;

        var msg   = self.chat.msg ( $scope.currentChat.target, $scope.msgText );
        var login = self.chat.acc.login;

        msg.text = $scope.msgText;
        msg.sender = login;
        $scope.currentChat._log.push(msg);

        $scope.msgText = "";
    }


    /**
     * Obsługa puszczenia przycisku w oknie wiadomości
     *    Głównie dla obsługi entera
     * @param  {KeyEvent} event
     */
    $scope.msgTextKeyup = function(event) {
        if (event.keyCode === 13 && event.ctrlKey === false)
            $scope.send_msg();
    }

    /**
     * Zamknięcie rozmowy
     * @param  {Object} chat Rozmowa do zakmnięcia
     */
    $scope.chat_close = function(chat) {
        for (var i in $scope.chats) {
            if ($scope.chats[i].target === chat.target) {
                self.chat.status.removeStatus(chat.target);
                if (chat.target === $scope.currentChat.target) {
                    $scope.currentChat = null;
                    $scope.showMsgBox  = false;
                }
                $scope.chats.splice(i, 1);
                break;
            }
        }
    }


    this.addEvent('new_msg');
    this.addEvent('login');
    this.addEvent('register');
    this.on('login', function(){
        self.login();
    });
    return this;
}

/**
 * Przełącza status zalogowania
 * @memberof Chat.Ctrl.ngapp
 */
Chat.Ctrl.ngapp.prototype.login = function() {
    var self = this;
    this.scope.$apply(function() {
        self.scope.loggedIn = true;
    });
}

/**
 * Odbiera wiadomości
 * @param  {_cmod.msg} msg Wiadomość
 * @memberof Chat.Ctrl.ngapp
 * @listens _cmod.sock#msg
 */
Chat.Ctrl.ngapp.prototype._msg = function(msg) {
    var self = this;
    var chat = false;
    for (var i in this.scope.chats) {
        if (msg.sender === this.scope.chats[i].target) {
            chat = self.scope.chats[i];
            break;
        }
    }

    if (chat === false) {
        chat = self.scope.new_chat(msg.sender);
    }

    this.scope.$apply(function(){
        if (self.scope.currentChat.target !== msg.sender)
            chat.unread++;
        chat._log.push(msg);
    });


    this.emit('msg', [ msg ] );
}

/**
 * Zmienia status użytkownika
 * @param  {String}  target Nazwa użytkownika
 * @param  {Boolean} status Nowy status
 * @listens _cmod.status#change
 */
Chat.Ctrl.ngapp.prototype._status = function(target, status) {
    for (var i in this.scope.chats) {
        var chat = this.scope.chats[i];
        if (target === chat.target) {
            this.scope.$apply(function(){
                chat.status = status;
            });
            break;
        }
    }
}



/**
 * Angularowy kontroler obsługi rejestracji
 * @param  {ChildScope} $scope  Angularowy $scope
 * @constructor
 * @memberof Chat.Ctrl
 */
Chat.Ctrl.ngreg = function($scope, wrap) {
    _csys.class.call(this);
    var self = this;
    this.scope = $scope;
    this.wrap = wrap;

    $scope.registering = false;

    $scope.close = function() {
        $scope.registering = false;
    }

    $scope.submit = function() {
        var sock = new _cmod.sock();
        var acc  = new _cmod.acc(sock);

        acc.register($scope.username, $scope.password).then(function(){
            alert('Zarejestrowano poprawnie!');
            self.clear();
            sock.close();
        }, function() {
            alert('Nie udało się zarejestrować! Spróbuj innej nazwy użytkownika.');
            sock.close();
        });
    }
}

/**
 * Czyszczenie po rejestracji
 * @memberof Chat.Ctrl.ngreg
 */
Chat.Ctrl.ngreg.prototype.clear = function() {
    var ss = this.scope;
    this.scope.$apply(function(){
        ss.password  = '';
        ss.password2 = '';
        ss.registering = false;
    });
}
