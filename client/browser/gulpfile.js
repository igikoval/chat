var gulp = require('gulp');

gulp.task('less', function() {
    var less = require('gulp-less'),
        path = require('path'),
        concat = require('gulp-concat');

    return gulp.src('files/less/*.less')
            .pipe(concat('style.css'))
            .pipe(less())
            .pipe(gulp.dest('files/css/'));
})
